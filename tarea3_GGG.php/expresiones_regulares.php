<html>
    <head>
        <title>Expresiones regulares</title>
        <meta charset="utf8">
    </head>

    <h1><strong><center>Expresiones regulares</center></strong></h1>
    <h6><center><i>Por Gerardo González Guerrero</i></center></h6>

    <body>  
    <p><strong>Expresión regular para correos </strong></p>
    <?php
    //Realizar una expresión regular que detecte emails correctos. 
    $str="gerardo_guerrero@hotmail.com"; //Correcto
    $result=preg_match("/^[a-zA-Z0-9\._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/", $str);
    echo $str;
    ?> </br>
    <?php
    echo "El resultado es ", $result, " y por lo tanto el correo es correcto";    
    //$str="gerardo.guerrerohotmail.com"; Incorrecto
    ?>

    <p><strong>Expresión regular para CURPS </strong></p>
    <?php
    //Realizar una expresion regular que detecte Curps Correctos ABCD123456EFGHIJ78. 
    $str = 'GOGG000315HDFNRRA5'; //Correcto
    $result = preg_match("/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/", $str);
    echo $str;
    ?> </br>
    <?php
    echo "El resultado es ", $result, " y por lo tanto el CURP es correcto";
    //$str="AVD13SDA332dV"; Incorrecto
    ?>

    <p><strong>Expresión regular para palabras de longitud mayor a 50 </strong></p>
    <?php
    //Realizar una expresion regular que detecte palabras de longitud mayor a 50 formadas solo por letras. 
    $str = 'Estacadenacontienemasdecincuentacaracteresyporesoesvalida'; //Correcta
    $result = preg_match("/^[a-zA-Z]{50,}$/", $str);
    echo $str;
    ?> </br>
    <?php
    echo "El resultado es ", $result, " y por lo tanto la palabra es correcta";  
    //$str="Estacadenaesmuycorta"; Incorrecto
    ?>

    <p><strong>Funcion para escapar simbolos especiales </strong></p>
    <?php
    //Crea una funcion para escapar los simbolos especiales. 
    $str = "Esta funcion permite quitar los simbolos especiales como estos: ' \ '";
    echo addslashes($str);

    ?>

    <p><strong>Expresión regular para números decimales </strong></p>
    <?php
    //Crear una expresion regular para detectar números decimales. 
    $str = '12.50'; //Correcta
    $result = preg_match("/^\d*\.?\d*$/", $str);
    echo $str; 
    ?> </br>
    <?php
    echo "El resultado es ", $result, " y por lo tanto el numero es correcto";  
    ?>

    </body>

    <style>

body {
    margin: 50px auto;
    text-align:center;
    width: 800px;
    font-size: 20px;
    font-family: "Arial";
}

p {
    border: 4px solid #ccc;
    font-weight: 100;
    font-family: 'Arial';
    padding: 8px;
    border-radius: 20px;
    background: #2EDFBC;
}
 
    </style>
</html>
